
export const strict = false
export const state = () => ({
  events: [
    { id: 0, title: 'Danish & Aiza Wedding', clientName: 'Danish Ali', clientContact: '03368361899', date: new Date().toLocaleString('en-GB'), team: 'Faizan Aslam ,Malik Faizan Zafar', location: 'Ayub Park', totalAmount: 10000, received: 5000, balance: 5000 },
    { id: 1, title: 'Kehan Birthday', clientName: 'Muhammad Ahmed', clientContact: '03368361899', date: new Date().toLocaleString('en-GB'), team: 'Kehan Ahmed, Muhammad Asad , Malik Faizan Zafar', location: 'G11 Markaz', totalAmount: 30000, received: 25000, balance: 5000 },
    { id: 2, title: 'Haseeb Graduation Ceremoney', clientName: 'Haseeb Ali', clientContact: '03368361899', date: new Date().toLocaleString('en-GB'), team: 'Faizan Aslam , Muhammad Asad', location: 'Islamic University', totalAmount: 20000, received: 5000, balance: 15000 },
    { id: 3, title: 'Al Jannat Mall Opening', clientName: 'Rauf Siddique', clientContact: '03368361899', date: new Date().toLocaleString('en-GB'), team: 'Kehan Ahmed , Muhammad Asad', location: 'Saddar', totalAmount: 30000, received: 23000, balance: 7000 }
  ],
  invoices: [],
  recentActivities: []
})

export const mutations = {
  toggleSidebar (state) {
    state.sidebar = !state.sidebar
  },
  addingInvoice (state, payload) {
    state.events[payload.eventID].received = state.events[payload.eventID].received + payload.amountPaid
    state.events[payload.eventID].balance = state.events[payload.eventID].balance - payload.amountPaid
    state.invoices.push(payload)
  },
  addingRecentActivity (state, payload) {
    state.recentActivities.push(payload)
  }
}
export const getters = {
  getAllEvents (state) {
    return state.events
  },
  getAllInvoices (state) {
    return state.invoices
  },
  getAllRecentActivities (state) {
    return state.recentActivities
  }
}
export const actions = {
  addInvoice ({commit}, payload) {
    commit('addingInvoice', payload)
  },
  addRecentActivity ({commit}, payload) {
    commit('addingRecentActivity', payload)
  },
  getEventInvoices ({commit}, payload) {
    commit('gettingEventInvoice', payload)
  }
}
