// import {firebaseAuth, fireStore} from '~/plugins/firebase.js'
import * as firebase from 'firebase'
require('firebase/firestore')
export const state = () => ({
  userData: null
})

export const mutations = {
  setUserData (state, payload) {
    state.userData = payload
  }
}
export const actions = {
  loadUserData ({commit}) {
    let usersData = {}
    return firebase.firestore().collection('userData').get().then(querySnapshot => {
      querySnapshot.forEach(doc => {
        let userData = {
          question: doc.data().question,
          answer: doc.data().answer
        }
        usersData[doc.id] = userData
      })
      commit('setUserData', usersData)
    })
  }
}

export const getters = {
  userData (state) {
    return state.userData
  }
}
