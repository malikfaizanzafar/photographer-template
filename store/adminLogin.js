// import {firebaseAuth, fireStore} from '~/plugins/firebase.js'
import * as firebase from 'firebase'
require('firebase/firestore')
export const state = () => ({
  user: null
})

export const mutations = {
  setUser (state, payload) {
    state.user = payload
  }
}
export const actions = {
  signUserInwithEmail ({commit}, payload) {
    // console.log('sign in methods initiated ', payload)
    const email = payload.email
    const password = payload.password
    return firebase.auth().signInWithEmailAndPassword(email, password)
      .then(data => {
        console.log('Login successful' + JSON.stringify(data))
        commit('setUser', data)
        return true
      }).catch(function (error) {
        var errorCode = error.code
        var errorMessage = error.message
        console.log('error is ' + JSON.stringify(errorCode) + 'error message is ' + JSON.stringify(errorMessage))
        commit('setUser', null)
        return error
      })
  },
  autoSignIn ({ commit }, payload) {
    const user = payload
    commit('setUser', user)
    this.$router.push('/admin')
    // })
  },
  logout ({ commit }) {
    return firebase.auth().signOut()
      .then(() => {
        this.$router.push('/login')
        commit('setUser', null)
      })
      .catch(error => {
        console.log(`An error occured Signing out the user. ${error}`)
      })
  }
}

export const getters = {
  user (state) {
    return state.user
  }
}
