
module.exports = {
  /*
  ** Headers of the page
  */
  mode: 'spa',
  head: {
    title: 'Project Template',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js + Vuetify.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { href: 'https://use.fontawesome.com/releases/v5.0.8/css/all.css', rel: 'stylesheet' }
    ]
  },
  plugins: [
    '@/plugins/vuetify.js',
    // '@/plugins/gmap.js',
    // '@/plugins/firebase-init.js',
    '@/plugins/sidebar',
    // '@/plugins/eCharts',
    '@/plugins/misc'
  ],
  css: [
    '~/assets/style/app.styl'
  ],
  modules: [
    ['qonfucius-nuxt-fontawesome', { componentName: 'fa-icon' }]
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: [
      '~/plugins/firebase-init.js',
      '~/plugins/vuetify.js'
    ],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      config.resolve.alias['vue'] = 'vue/dist/vue.common'
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
