import {Bar} from 'vue-chartjs'
export default {
  extends: Bar,
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: [
        {
          label: 'Receieved Amount',
          backgroundColor: '#28b463',
          data: [50000, 50000, 50000, 30000, 40000, 35000, 30000, 40000, 50000, 50000, 30000, 30000]
        },
        {
          label: 'Remaining Amounts',
          backgroundColor: '#cd6155',
          data: [30000, 20000, 30000, 40000, 30000, 45000, 39000, 20000, 40000, 25000, 40000, 50000]
        }
      ]
    })
  }
}
