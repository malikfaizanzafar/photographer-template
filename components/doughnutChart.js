import {Doughnut} from 'vue-chartjs'
export default {
  extends: Doughnut,
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: ['Haseeb Ahmed', 'Muhammad Asad', 'Malik Faizan Zafar', 'Kehan Ahmed', 'Faizan Aslam'],
      datasets: [
        {
          label: 'Work Distribution',
          backgroundColor: [
            '#FF6384',
            '#63FF84',
            '#84FF63',
            '#8463FF',
            '#6384FF'
          ],
          data: [4, 6, 8, 7, 5]
        }
      ]
    })
  }
}
