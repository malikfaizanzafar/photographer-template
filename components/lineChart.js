import {Line} from 'vue-chartjs'
export default {
  extends: Line,
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: [
        {
          label: 'Number of Events',
          borderColor: '#0294ca',
          backgroundColor: 'rgba(0, 0, 0, 0)',
          data: [20, 10, 12, 15, 11, 8, 19, 20, 10, 14, 18, 5]
        }
      ]
    })
  }
}
