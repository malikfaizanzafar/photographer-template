module.exports = {
  root: true,
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true,
    "commonjs": true,
    "es6": true,
  },
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  rules: {
    "no-const-assign": "warn",
    "no-return-assign": "off",
    "no-unused-vars": "off"
  }
}
