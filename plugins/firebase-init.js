import * as firebase from 'firebase/app'
import 'firebase/auth'

// Add your Firebase configuration here
var config = {
  apiKey: 'AIzaSyBTFbwaEdwvHqVhrb78wwLNXaaY4Nj2nDs',
  authDomain: 'photo2-d57cc.firebaseapp.com',
  databaseURL: 'https://photo2-d57cc.firebaseio.com',
  projectId: 'photo2-d57cc',
  storageBucket: '',
  messagingSenderId: '925626557869'
}

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app()
export const Auth = firebase.auth()
