import Vue from 'vue'

// libraries
import FontAwesomeIcon from '@fortawesome/vue-fontawesome'
import fontawesome from '@fortawesome/fontawesome'
import brands from '@fortawesome/fontawesome-free-brands'
import fa from '@fortawesome/fontawesome-free-solid'
// import Chartist from 'chartist'
// import 'bootstrap/dist/css/bootstrap.css'
// import '@/assets/sass/paper-dashboard.scss'
import 'es6-promise/auto'
import VueGoodTable from 'vue-good-table'
// import DateFormate from 'dateformat'
// components
import Notifications from '@/components/Admin/UIComponents/NotificationPlugin'

fontawesome.library.add(brands, fa)
Vue.use(Notifications)
Vue.use(VueGoodTable)
// Vue.use(DateFormate)
Vue.component('font-awesome-icon', FontAwesomeIcon)
