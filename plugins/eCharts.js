import Vue from 'vue'
import ECharts from '~/node_modules/vue-echarts/components/ECharts'

// import ECharts modules manually to reduce bundle size
import 'echarts/lib/chart/bar'
import 'echarts/lib/chart/line'
import 'echarts/lib/chart/pie'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/polar'
import 'echarts/lib/component/tooltip'

// register component to use
Vue.component('chart', ECharts)
