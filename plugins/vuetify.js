import Vue from 'vue'
import Vuetify from 'vuetify'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'
import Lightbox from 'vue-simple-lightbox'
import 'vue-event-calendar/dist/style.css'
import vueEventCalendar from 'vue-event-calendar'
import VCalendar from 'v-calendar'
import 'v-calendar/lib/v-calendar.min.css'
import StarRating from 'vue-star-rating'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCN9Fr-WHMtqsL5k4HPPX3Vp7E2TYoO2nQ',
    libraries: 'places'
  }
})
Vue.use(Lightbox)
Vue.use(Vuetify)
Vue.use(vueEventCalendar, {
  locale: 'en',
  color: '#0294ca',
  className: 'selected-day'
})
Vue.use(VCalendar, {
  firstDayOfWeek: 2 // Monday
})
Vue.component('star-rating', StarRating)
